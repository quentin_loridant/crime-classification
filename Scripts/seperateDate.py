#!/usr/bin/python
# -*- coding: latin-1 -*-


import numpy as np 
import cv2
import csv
import sys
import re

def seperateDateTime(filename):
	fileCSV = open(filename, "r")
	# Extract the first attribute (date) and separate it with regular expression
	# Date format : 2003-04-05 20:02:00
	newFile = []
	# Getting the names of classes and editing it
	firstLine = fileCSV.readline()
	patternDate = r"Dates"
	firstLine = re.sub(patternDate, "Date,Time", firstLine, flags = 0)
	# Adding a comma after the date to seperate
	for line in fileCSV:
		patternDate = r"[0-9]{4}-[0-9]{2}-[0-9]{2}"
		date = re.findall(patternDate, line, flags=0)
		if date: # Not empty List (mainly for the first line of the file)
			newFile.append(re.sub(patternDate, date[0]+r",", line, flags = 0))
	fileCSV.close()

	# Writting in the new file
	newfileCSV = open("generatedFile.csv", "w")

	newfileCSV.write(firstLine)
	for line in newFile:
		newfileCSV.write(line)

	newfileCSV.close()
	return newFile


seperateDateTime("trainMini.csv")
