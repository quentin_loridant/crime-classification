# Main
# Tanguy Esteoule, Quentin 

import numpy as np
import cv2
import os
import re
import weka.core.jvm as jvm
from weka.core.converters import Loader



def init():
	jvm.start()

	#WARNING : data must be sorted by category !!
	data = loadData("file2.csv")
	data.class_is_last() # TODO
	test = loadData("test1.csv")
	f = initCSV("resultats.csv")

	return (data, test, f)

def end():
	jvm.stop()

def loadData(filename):
	#data_dir = "/home/quentin/Documents/SF/datasets/" TODO
	data_dir = "/home/tango/ENSIMAG/RAIN/"
	loader = Loader(classname="weka.core.converters.CSVLoader")
	data = loader.load_file(data_dir + filename)
	return data
	
def initCSV(archivo):
	f = open(archivo, 'w')
	f.write("Id,ARSON,ASSAULT,BAD CHECKS,BRIBERY,BURGLARY,DISORDERLY CONDUCT,DRIVING UNDER THE INFLUENCE,DRUG/NARCOTIC,DRUNKENNESS,EMBEZZLEMENT,EXTORTION,FAMILY OFFENSES,FORGERY/COUNTERFEITING,FRAUD,GAMBLING,KIDNAPPING,LARCENY/THEFT,LIQUOR LAWS,LOITERING,MISSING PERSON,NON-CRIMINAL,OTHER OFFENSES,PORNOGRAPHY/OBSCENE MAT,PROSTITUTION,RECOVERED VEHICLE,ROBBERY,RUNAWAY,SECONDARY CODES,SEX OFFENSES FORCIBLE,SEX OFFENSES NON FORCIBLE,STOLEN PROPERTY,SUICIDE,SUSPICIOUS OCC,TREA,TRESPASS,VANDALISM,VEHICLE THEFT,WARRANTS,WEAPON LAWS\n")
	return f

def naivesBayesModel(data, test_data, f):
	from weka.classifiers import Classifier
	cls = Classifier(classname="weka.classifiers.tree.J48")
	# Construction of the model
	cls.build_classifier(data)
	
	# Class in right order
	arrayClass = ["ARSON","ASSAULT","BAD CHECKS","BRIBERY","BURGLARY","DISORDERLY CONDUCT","DRIVING UNDER THE INFLUENCE","DRUG/NARCOTIC","DRUNKENNESS","EMBEZZLEMENT","EXTORTION","FAMILY OFFENSES","FORGERY/COUNTERFEITING","FRAUD","GAMBLING","KIDNAPPING","LARCENY/THEFT","LIQUOR LAWS","LOITERING","MISSING PERSON","NON-CRIMINAL","OTHER OFFENSES","PORNOGRAPHY/OBSCENE MAT","PROSTITUTION","RECOVERED VEHICLE","ROBBERY","RUNAWAY","SECONDARY CODES","SEX OFFENSES FORCIBLE","SEX OFFENSES NON FORCIBLE","STOLEN PROPERTY","SUICIDE","SUSPICIOUS OCC","TREA","TRESPASS","VANDALISM","VEHICLE THEFT","WARRANTS","WEAPON LAWS"]

	# Prediction and creation of the submission
	indiceInst = 0;
	for inst in test_data:
		tableau = cls.distribution_for_instance(inst)
		f.write(str(indiceInst))
		indiceClass = 0;
		indiceClass2 = 0;

		# If we don't have all the classes in our train
		for c in arrayClass:
			if(len(data.class_attribute.values) > indiceClass2):
				if(data.class_attribute.values[indiceClass2] == c or data.class_attribute.values[indiceClass2] == "'"+c+"'"):
					f.write("," + str(tableau[indiceClass2]))
					indiceClass2 += 1
				else:
					f.write(",0")
			else:
				f.write(",0")

			indiceClass += 1

			# For the last column
			if(indiceClass == len(arrayClass)):
				f.write("\n")
		indiceInst += 1;

	return cls

def naivesBayesEvaluation(data):
	from weka.classifiers import Classifier, Evaluation
	from weka.core.classes import Random
	cls = Classifier(classname="weka.classifiers.trees.J48", options=["-C", "0.3"])
	cls.build_classifier(data)
	print (cls)
	eval = Evaluation(data, cost_matrix = None)
	eval.crossvalidate_model(cls, data, 10, Random(1), output=None)
	print (eval.summary())
	
	import weka.plot.graph as graph
	graph.plot_dot_graph(cls.graph)
	return cls

def main():
	data, test_data, f = init()
	# cls = naivesBayesModel(data, test_data, f)
	cls = naivesBayesEvaluation(data)
	end()


main()
