# Project at PUCP : How to predict a crime�s category using Weka tools for machine learning.

This project was realized by Quentin Loridant and Tanguy Esteoule. It is a scientific project more than a compute science one.
Therefore we emphasized our work in the scientific analysis. An article was written to describe our work and our results.

This article was presented at a artifical intelligence workshop in 2015 at PUCP by our teachers and advisors Cesar Baltran and Hugo Alatrista.

It is available in the *"Downloads" section* of this bitbucket project. 