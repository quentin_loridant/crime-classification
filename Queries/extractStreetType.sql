UPDATE sanfrancisco.train
SET TypeOfStreet = CASE
	WHEN INSTR(Address, 'ST') > 0 THEN 'Street'
	WHEN INSTR(Address, 'AV') > 0 THEN 'Avenue'
	WHEN INSTR(Address, 'BL') > 0 THEN 'Boulevard'
	WHEN INSTR(Address, 'RD') > 0 THEN 'Road'
	ELSE 'Street' 
    END;