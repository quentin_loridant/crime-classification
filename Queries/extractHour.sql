UPDATE sanfrancisco.train
SET MomentOfDay = CASE
	WHEN 5 <= Hour(Date) && Hour(Date) < 12 THEN 'morning'
    WHEN 12 <= Hour(Date) && Hour(Date) < 20  THEN 'day'
    WHEN 20 <= Hour(Date) OR Hour(Date) < 5 THEN 'night'
    ELSE 'day'
    END;