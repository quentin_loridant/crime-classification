UPDATE sanfrancisco.train
SET Season = CASE
	WHEN 1 <= month(Date) && month(Date) <= 3 THEN 'winter'
    WHEN 4 <= month(Date) && month(Date) <= 6  THEN 'spring'
    WHEN 7 <= month(Date) && month(Date) <= 9 THEN 'summer'
	WHEN 10 <= month(Date) && month(Date) <= 12 THEN 'automn'
	ELSE 'spring'
    END;